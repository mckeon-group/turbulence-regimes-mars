# Turbulence-Regimes-Mars

Codes:
- Main data analysis: "turbulenceRegimesMars_MainAnalysis.ipynb"

- Formatting data: "formatSols_pressure.ipynb", "formatSols.ipynb"

- File containing functions used in data formatting: "functions_dataFormat.py"

The raw data was obtained from https://atmos.nmsu.edu/ under the calibrated data link: https://atmos.nmsu.edu/PDS/data/PDS4/Mars2020/mars2020_meda/data_calibrated_env/. Data set citation: J.A. Rodriguez-Manfredi et al. (2021), The Mars Environmental Dynamics Analyzer, MEDA, NASA Planetary Data System, DOI 10.17189/1522849.

In this analysis, data from sols 100-150 were downloaded.
The purpose of this work was to use classical turbulence scaling of the wind speed spectra to determine the turbulence regimes that can be captured by the Mars Environmental Dynamics Analyzer (MEDA). The codes here accompany the work titled "On the universality of turbulence scaling in the Martian atmosphere" and produce all of the figures (and supplemental figures) used in the manuscript.

## Requirements
python 3.11.2

numpy 1.23.5

scipy 1.10.0

pandas 1.5.3

