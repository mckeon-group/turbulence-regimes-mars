# Functions used to format the sol data. Includes:
#       convert_LMST()
#       convert_LTST()


# define a function that converts the LTST times into fractions of an hour. 
def convert_LTST(ltst):
    hours = []
    for l in ltst:
        nums = l[5:].split(":")
        nums_float = [float(n) for n in nums]
        hours += [nums_float[0] + nums_float[1]/60 + nums_float[2]/3600]
    return hours


# define a function that converts the LTST times into fractions of an hour. 
def convert_LMST(lmst):
    hours = []
    for l in lmst:
        nums = l[6:].split(":")
        nums_float = [float(n) for n in nums]
        hours += [nums_float[0] + nums_float[1]/60 + nums_float[2]/3600]
    return hours


# define a function that separate the data into distinct data collection intervals
def separate_ints(data):
    t = data["SCLK"]
    #u = data["WS1"]

    #LTST = data["LTST"]
    #LMST = data["LMST"]

    # Determine location of intervals
    ind = [0]
    for d in range(len(t.iloc[0:-1])):
        if t.iloc[d] != t.iloc[d+1]-0.5:
            ind += [d] 

    # Split into intervals: int
    data_int = {}
    startPoint = ind[0]
    for x in range(len(ind)-1): 
        endPoint = ind[x+1] + 1
        data_int["{0}".format(x+1)] = data[startPoint:endPoint]
        startPoint = endPoint + 1
    
    # Returns a dict where the keys are the various interval numbers
    return data_int

# define a function to remove nan values from a subset of data, and delete the entire row
def removeSubsetNan(data_int, subsetName):
    for i in (range(len(data_int))):
        data_int[f'{i+1}'] = data_int[f'{i+1}'].dropna(subset = subsetName)
    return data_int


    
